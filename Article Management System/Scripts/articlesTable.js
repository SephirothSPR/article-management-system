﻿$(document).ready(function () {

    loadData();
    
});

function loadData() {
    $.ajax("http://localhost:5050/api/articlesapi")
    .done(function (response) {

    //Set the variable aaData to the response
    var aaData = response;

    $('#articlesTable').dataTable({
        "aaData": aaData,
        "aoColumns": [
            { "mDataProp": "id", "bSortable": false, "bSearchable": false, "bVisible": false },
            { "mDataProp": "title" },
            { "mDataProp": "description", "bSearchable": false },
            { "mDataProp": "date", "bSortable": false, "bSearchable": false },
            {
                mRender: function (data, type, row) {
                    //return '<a type="button" class="edit btn btn-warning pull-right" style="width: 80px" href="/Home/EditArticle?id=' + row['id'] + '">EDIT</a>'
                    return '<button type="button" class="editArt btn btn-warning pull-right" style="width: 80px" name="EButton" id="' + row['id'] + '">EDIT</button>'
                },"bSearchable": false
            },
            {
                mRender: function (data, type, row) {
                    //return '<a type="button" class="del btn btn-danger pull-right" style="width: 80px" href="/Home/DeleteArticle?id=' + row['id'] + '">DELETE</a>'
                    return '<button type="button" class="delArt btn btn-danger pull-right" style="width: 80px" name="DButton" id="' + row['id'] + '">DELETE</button>'
                }, "bSearchable": false
            },
            {
                mRender: function (data, type, row) {
                    //return '<a type="button" class="del btn btn-danger pull-right" style="width: 80px" href="/Home/DeleteArticle?id=' + row['id'] + '">DELETE</a>'
                    //return '<button type="button" class="detailArt btn btn-info pull-right" style="width: 80px" name="DTButton" id="' + row['id'] + '">DETAIL</button>'
                    return '<a type="button" class="del btn btn-info pull-right" style="width: 80px" href="/Home/ShowArticle?id=' + row['id'] + '">DETAIL</a>'
                }, "bSearchable": false
            }
        ]
    });
})
}