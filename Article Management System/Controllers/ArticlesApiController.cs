﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Article_Management_System.Controllers
{
    public class ArticlesApiController : ApiController
    {
        Models.ArticlesDbContext ArticlesDb = new Models.ArticlesDbContext();

        public IEnumerable<Models.ArticleModels> GetAllArticles()
        {
            return ArticlesDb.Articles.ToArray();
        }

        public IHttpActionResult GetArticle(DateTime id)
        {
            var article = ArticlesDb.Articles.Where((a) => a.date <= id);
            if(article == null)
            {
                return NotFound();
            }
            return Ok(article);
        }
    }
}
