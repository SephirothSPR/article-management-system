﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Article_Management_System.Controllers
{
    public class HomeController : Controller
    {
        private Models.ArticlesDbContext ArticleDb = new Models.ArticlesDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Articles()
        {
            return View();
        }

        public ActionResult ArticlesRO()
        {
            return View();
        }

        public ActionResult GetArticle(int? id)
        {
            var ArticlesDb = new Models.ArticlesDbContext();
            var article = ArticlesDb.Articles.FirstOrDefault((a) => a.id == id);
            return Json(article, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowArticle(String id)
        {
            Models.ArticleModels articleToShow = ArticleDb.Articles.Find(int.Parse(id));
            return View(articleToShow);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddArticle(String mtitle, String mdesc, String mdate)
        {
            var articleToAdd = new Models.ArticleModels();
            articleToAdd.title = mtitle;
            articleToAdd.description = mdesc;

            try
            {
                articleToAdd.date = DateTime.Parse(mdate);
            }
            catch { }

            ArticleDb.Articles.Add(articleToAdd);
            ArticleDb.SaveChanges();

            var result = true;
            return Content(result ? "OK" : String.Empty);
        }

        [Authorize]
        public ActionResult EditArticle(String mid, String mtitle, String mdesc, String mdate)
        {
            var result = false;
            Models.ArticleModels articleToEdit = ArticleDb.Articles.Find(int.Parse(mid));
            if (articleToEdit != null)
            {
                articleToEdit.title = mtitle;
                articleToEdit.description = mdesc;
                articleToEdit.date = DateTime.Parse(mdate);
                ArticleDb.Articles.Attach(articleToEdit);

                var entry = ArticleDb.Entry(articleToEdit);
                entry.Property(e => e.title).IsModified = true;
                entry.Property(e => e.date).IsModified = true;
                entry.Property(e => e.description).IsModified = true;

                ArticleDb.SaveChanges();
                result = true;
            }

            return Content(result ? "OK" : String.Empty);
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteArticle(String mid)
        {
            int aid = int.Parse(mid);
            Models.ArticleModels articleToDelete = ArticleDb.Articles.FirstOrDefault(a => a.id == aid);
            if (articleToDelete != null)
            {
                ArticleDb.Articles.Remove(articleToDelete);
                ArticleDb.SaveChanges();
            }
            var result = true;
            return Content(result ? "OK" : String.Empty);
        }
    }
}