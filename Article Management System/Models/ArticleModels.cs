﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Article_Management_System.Models
{
    public class ArticleModels
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
    }

    public class ArticlesDbContext : DbContext
    {
        public DbSet<ArticleModels> Articles { get; set; }
    }
}